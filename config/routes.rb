Rails.application.routes.draw do

	devise_for :users, :controllers => {:omniauth_callbacks => "omniauth_callbacks"}
	resources :users do
		member do
			get "profile"
			post "status"

			get "profile/edit" => "users#edit"
		end

		collection do
			post "follow"
		end
	end

	root "pages#home"
	
	get "/home" => "pages#home"
	get "/admin" => "pages#admin"

	get "/about" => "pages#about"
	get "/contact" => "pages#contact"
	get "/faq" => "pages#faq"
	get "/privacy" => "pages#privacy"
	
	post "/withdraw" => "pages#withdraw"

	resources :videos do
		collection do
			get "setting"
			get "favourite"

			get "spoop_of_the_month"
			get "spoop_of_the_year"
			
			get "flagged"

			post "rate"

			get "search"
		end

		member do
			get "like"
			
			get "report_abuse"
			get "remove_abuse_flag"
		end
	end

	resources :comments

	resources :themes
	
	resources :messages do
		collection do
			get "inbox"
			get "sent"
			get "trash"
		end
	end
	
	resources :pledges do
		collection do
			get "cart"
			post "charges"
		end
	end

	resources :subscriptions

	resources :channels do
		member do
			get "subscribe"
		end
	end

	resources :user_status_comments

	resources :user_statuses do
		member do
			get "like"
			get "share"
		end
	end
end
