MAILER_CONFIG = YAML.load_file("#{Rails.root}/config/mailer.yml")[Rails.env].symbolize_keys
STRIPE_CONFIG = YAML.load_file("#{Rails.root}/config/stripe.yml")[Rails.env].symbolize_keys
SETTINGS_CONFIG = YAML.load_file("#{Rails.root}/config/settings.yml")[Rails.env].symbolize_keys
FACEBOOK_CONFIG = YAML.load_file("#{Rails.root}/config/facebook.yml")[Rails.env].symbolize_keys
GOOGLE_CONFIG = YAML.load_file("#{Rails.root}/config/google.yml")[Rails.env].symbolize_keys