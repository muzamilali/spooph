Rails.application.config.assets.precompile += %w( video-js.swf vjs.eot vjs.svg vjs.ttf vjs.woff )

Rails.application.config.assets.precompile += %w( progress_bar.js )
Rails.application.config.assets.precompile += %w( jquery_bottom.js )