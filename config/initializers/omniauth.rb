Rails.application.config.middleware.use OmniAuth::Builder do
	
	provider :facebook, FACEBOOK_CONFIG[:app_id], FACEBOOK_CONFIG[:app_secret],
		:scope => "email,user_birthday,read_stream"

	provider :google_oauth2, GOOGLE_CONFIG[:app_id], GOOGLE_CONFIG[:app_secret]
end