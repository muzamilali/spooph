class SubscriptionsController < ApplicationController
	before_action :set_new_subscription, :only => [:new]

	def new
	end

	def create
		if current_user.have_subscription_money? || charged_by_card?
			user = current_user
			today = Date.today

			Transaction.create!(:user_id => user.id, :amount => SETTINGS_CONFIG[:spooph_subscription_fee].to_f.round(2), :debit => true, :description => "Spooph Subscription Fee")
			subscription = Subscription.create!(:user_id => user.id, :start => today, :end => today + SETTINGS_CONFIG[:paid_subscription_duration].to_i.days)

			SubscriptionMailer.paid_subscription(subscription)

			flash[:notice] = "Subscription was successfully created."
		end

		respond_to do |format|
			format.html { redirect_to root_path }
		end
	end

	private
	def charged_by_card?
		token = params[:stripeToken]
		amount = SETTINGS_CONFIG[:spooph_subscription_fee].to_f.round(2) * 100
		response = false

		begin
			if token
				charge = Stripe::Charge.create(
					:amount => amount.to_i,
					:currency => "usd",
					:source => token,
					:description => "Spooph Subscription Fee"
				)

				response = true
			end
		rescue Stripe::CardError => ex
			response = false
		end

		response
	end


	def set_new_subscription
		@subscription = Subscription.new
	end
end
