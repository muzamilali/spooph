class CommentsController < ApplicationController
	before_action :set_comment, only: [:update, :destroy]

	def create
		comment = Comment.new(comment_params)
		comment.save!

		html = render_to_string :partial => "comments/users_comment", :locals => {:video => comment.video}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	def update
		@comment.message = comment_params[:message]  if @comment.user == current_user
		@comment.save!

		html = render_to_string :partial => "comments/users_comment", :locals => {:video => @comment.video}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	def destroy
		@comment.removed = true  if @comment.user == current_user
		@comment.save!

		html = render_to_string :partial => "comments/users_comment", :locals => {:video => @comment.video}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	private
	def set_comment
		@comment = Comment.find(params[:id])
	end

	def comment_params
		params.require(:comment).permit(:message, :video_id, :parent_id).merge(user_id: current_user.id)
	end
end
