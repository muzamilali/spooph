include ApplicationHelper
class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception

	helper_method :resource_name, :resource, :devise_mapping

	before_action :configure_permitted_parameters, :if => :devise_controller?

	before_action :authenticate_user!
	before_action :set_themes
	before_action :set_new_video

	def after_sign_in_path_for(resource)
		current_user.admin ? admin_path : root_path
	end

	def after_sign_up_path_for(resource)
		after_sign_in_path_for(resource)
	end

	def set_themes
		@themes = Theme.all
	end

	def set_new_video
		@new_video = Video.new
	end

	def check_for_subscribed_user?
		current_user and current_user.has_active_subscription?
	end

	protected

	def configure_permitted_parameters
		devise_parameter_sanitizer.for(:sign_up) << [:username, :name, :bio, :photo]
	end

	private

	def authenticate_admin!
		unless current_user.admin
			flash[:alert] = "You are not authorized to access this page"
			redirect_to root_path
		end
	end
end
