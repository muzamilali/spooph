class MessagesController < ApplicationController
	before_action :set_message, only: [:show, :destroy]
	before_action :mark_read, only: [:show]

	def show
	end

	def create
		@message = Message.new(message_params)

		respond_to do |format|
			if @message.save
				format.html { redirect_to @message, notice: 'Message was successfully created.' }
				format.json { render :show, status: :created, location: @message }
			else
				format.html { render :new }
				format.json { render json: @message.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@title = "Trash Messages"
		@messages = []
		redirect_path = trash_messages_path

		if @message.recipient == current_user
			@title = "Inbox Messages"

			@message.update(:recipient_trashed => true)
			@messages = current_user.received_messages.remove_deleted_inbox_messages

			redirect_path = inbox_messages_path
		elsif @message.sender == current_user
			@title = "Sent Messages"

			@message.update(:sender_trashed => true)
			@messages = current_user.sent_messages.remove_deleted_sent_messages

			redirect_path = sent_messages_path
		end

		html = render_to_string :template => "messages/messages_list", :locals => {:messages => @messages}, :layout => false
		respond_to do |format|
			format.html { redirect_to redirect_path }
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	def inbox
		@title = "Inbox Messages"
		@messages = current_user.received_messages.remove_deleted_inbox_messages

		render "messages_list"
	end

	def sent
		@title = "Sent Messages"
		@messages = current_user.sent_messages.remove_deleted_sent_messages

		render "messages_list"
	end

	def trash
		@title = "Trash Messages"
		@messages = Message.only_deleted_messages(current_user)

		render "messages_list"
	end

	private
	def set_message
		@message = Message.find(params[:id])
	end

	def mark_read
		@message.update(:read => true) if current_user == @message.recipient
	end

	def message_params
		params.require(:message).permit(:subject, :body, :recipient_id).merge(sender_id: current_user.id)
	end
end
