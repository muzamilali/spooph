class PagesController < ApplicationController

	skip_before_action :authenticate_user!, :only => [:home, :about, :contact, :privacy, :faq]
	
	before_action :authenticate_admin!, :only => [:admin]
	
	def home
		@main_video = User.subscribed_users_videos.sample || Video.most_viewed_videos[0..4].sample

		impressionist(@main_video, "video hit") if @main_video

		@right_panel_videos = Video.all - [@main_video]
		@recently_viewed_videos = current_user ? current_user.recently_viewed_videos : []
		@fresh_videos = Video.last(8)
	end

	def admin
	end

	def withdraw
		amount = params[:amount].to_f.round(2)

		if current_user.money_owned <= 0 or current_user.money_owned < amount
			flash[:notice] = "You don't have enough money to withdraw."
			redirect_to profile_user_path(current_user)
		elsif current_user.account.nil? || current_user.account.empty? || current_user.routing_number.nil? || current_user.routing_number.empty?
			flash[:notice] = "Please setup your bank details first."
			redirect_to profile_edit_user_path(current_user)
		else
			amount_to_withdraw = deduct_retention_before_withdraw(amount)
			remaining_amount = stripe_withdraw_fee_adjustment(amount_to_withdraw) * 100

			if remaining_amount > 0

				token = Stripe::Token.create(:bank_account => {
					:country => "US",
					:routing_number => current_user.routing_number,
					:account_number => current_user.account
				})

				recipient = Stripe::Recipient.create(
					:name => "#{current_user.name} - #{current_user.email}",
					:email => current_user.email,
					:type => "individual",
					:bank_account => token.id
				)

				transfer = Stripe::Transfer.create(
					:amount => remaining_amount.to_i,
					:currency => "usd",
					:recipient => recipient.id,
					:description => "#{current_user.email} withdrawing pledge money #{amount}"
				)
	
				current_user.add_debit_amount(amount) unless transfer["failure_code"]
				flash[:notice] = "You will recieve your payment within week."
			else
				flash[:notice] = "Amount is too low to withdraw."
			end

			redirect_to profile_user_path(current_user)
		end
	end

	def about
	end

	def contact
	end

	def faq
	end
	
	def privacy
	end
end
