class UsersController < ApplicationController
	before_action :set_user, only: [:profile, :update]

	def profile
	end

	def edit
		@user = current_user
	end

	def update
		if params[:user][:photo]
			@user.photo = params[:user][:photo]
		end

		if params[:user][:banner]
			@user.banner = params[:user][:banner]
		end

		if (!params[:user][:password].empty?) and params[:user][:password_confirmation] and params[:user][:password] == params[:user][:password_confirmation]
			@user.password = @user.password_confirmation = params[:user][:password]
		end

		@user.assign_attributes(user_params)

		respond_to do |format|
			if @user.save
				sign_in(@user, :bypass => true)
				format.html { redirect_to profile_user_path(@user), notice: 'Your profile has been successfully updated.' }
			else
				format.html { render :edit }
			end
		end
    end

	def follow
		follow = UserFollowing.where(follow_params).first_or_initialize
		follow.save!

		respond_to do |format|
			format.html { redirect_to profile_user_path(follow.user_id), notice: "Success" }
		end
	end

	def status
		status = UserStatus.new(user_status_params)
		status.save! unless status.body.squish.empty?

		redirect_to profile_user_path(current_user)
	end

	private
	def set_user
		@user = User.find(params[:id])
	end

	def follow_params
		params.permit(:user_id).merge(:followed_by => current_user.id)
	end

	def user_status_params
		params.permit(:body).merge(:user_id => current_user.id)
	end

	def user_params
		params.require(:user).permit(:username, :name, :bio, :account, :routing_number, :country)
	end
end
