class UserStatusCommentsController < ApplicationController
	before_action :set_comment, only: [:update, :destroy]

	def create
		comment = UserStatusComment.new(status_comment_params)
		comment.save!

		html = render_to_string :partial => "user_status_comments/users_comment", :locals => {:status => comment.user_status}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	def update
		@comment.message = status_comment_params[:message]  if @comment.user == current_user
		@comment.save!

		html = render_to_string :partial => "user_status_comments/users_comment", :locals => {:status => @comment.user_status}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	def destroy
		@comment.removed = true  if @comment.user == current_user
		@comment.save!

		html = render_to_string :partial => "user_status_comments/users_comment", :locals => {:status => @comment.user_status}, :layout => false
		respond_to do |format|
			format.json { render :json => {:content => html, :status => 200 } }
		end
	end

	private
	def set_comment
		@comment = UserStatusComment.find(params[:id])
	end

	def status_comment_params
		params.require(:user_status_comment).permit(:message, :user_status_id, :parent_id).merge(user_id: current_user.id)
	end
end
