require "will_paginate/array"
class ThemesController < ApplicationController
	before_action :set_theme, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_admin!, :except => [:show]

    skip_before_action :authenticate_user!, :only => [:show]

	def index
		@themes = Theme.all
	end

	def show
		@videos = @theme.videos.paginate(:page => params[:page], :per_page => 10)
	end

	def new
		@theme = Theme.new
	end

	def edit
	end

	def create
		@theme = Theme.new(theme_params)

		respond_to do |format|
			if @theme.save
				format.html { redirect_to @theme, notice: 'Theme was successfully created.' }
				format.json { render :show, status: :created, location: @theme }
			else
				format.html { render :new }
				format.json { render json: @theme.errors, status: :unprocessable_entity }
			end
		end
	end

	def update
		respond_to do |format|
			if @theme.update(theme_params)
				format.html { redirect_to @theme, notice: 'Theme was successfully updated.' }
				format.json { render :show, status: :ok, location: @theme }
			else
				format.html { render :edit }
				format.json { render json: @theme.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@theme.destroy
		respond_to do |format|
			format.html { redirect_to themes_url, notice: 'Theme was successfully destroyed.' }
			format.json { head :no_content }
		end
	end

	private
	def set_theme
		@theme = Theme.find(params[:id])
	end

	def theme_params
		params.require(:theme).permit(:name, :html_class)
	end
end
