class PledgesController < ApplicationController
	before_action :set_pledge, only: [:update, :destroy]

	def create
		@pledge = Pledge.new(pledge_params)
		@pledge.amount = 2.0

		respond_to do |format|
			if @pledge.save
				format.html { redirect_to video_path(@pledge.video), notice: "Pledge was successfully created." }
			else
				format.html { render :new }
			end
		end
	end

	def update
		respond_to do |format|
			if @pledge.update(pledge_params) and @pledge.user == current_user
				format.html { redirect_to cart_pledges_path, notice: "Pledge was successfully updated." }
				format.json { render :show, status: :ok, location: @pledge }
			else
				format.html { render :edit }
				format.json { render json: @pledge.errors, status: :unprocessable_entity }
			end
		end
	end

	def destroy
		@pledge.destroy if @pledge.user == current_user
		respond_to do |format|
			format.html { redirect_to cart_pledges_path, notice: "Pledge was successfully destroyed." }
			format.json { head :no_content }
		end
	end

	def cart
		@pledges = current_user.unpaid_pledges
	end

	def charges
		amount = calculate_strip_money(current_user.unpaid_pledges_amount)
		token = params[:stripeToken]

		begin
			charge = Stripe::Charge.create(
				:amount => amount,
				:currency => "usd",
				:source => token,
				:description => STRIPE_CONFIG[:title]
			)

			current_user.mark_unpaid_pledges
			notice = "Successfully paid."
		rescue Stripe::CardError => ex
			notice = "Error in verification."
		end

		respond_to do |format|
			format.html { redirect_to cart_pledges_path, notice: notice }
		end
	end

	private
	def set_pledge
		@pledge = Pledge.find(params[:id])
	end

	def pledge_params
		params.require(:pledge).permit(:channel_id, :video_id, :amount).merge(:user_id => current_user.id)
	end
end