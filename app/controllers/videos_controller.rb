class VideosController < ApplicationController
	before_action :set_video, only: [:show, :edit, :update, :destroy, :report_abuse, :remove_abuse_flag, :like]
	before_action :authenticate_admin!, :only => [:flagged, :remove_abuse_flag]

	skip_before_action :authenticate_user!, :only => [:show, :search, :spoop_of_the_month, :spoop_of_the_year]
	skip_before_action :verify_authenticity_token, :only => :rate

	def show
		@main_video = @video

		impressionist(@main_video, "video hit") if @main_video
		
		@right_panel_videos = Video.all - [@main_video]
		@recently_viewed_videos = current_user ? current_user.recently_viewed_videos : []
		@fresh_videos = Video.last(8)

		render "pages/home"
	end

	def edit
	end

	def create
		@video = Video.new(video_params)
		@video.themes << themes_collection

		respond_to do |format|
			if @video.save
				format.html { redirect_to video_path(@video), notice: "Video was successfully uploaded." }
			else
				@new_video = @video
				format.html { render :new }
			end
		end
	end

	def update
		respond_to do |format|
			if (@video.user == current_user || current_user.admin) and @video.update(video_params)
				format.html { redirect_to @video, notice: "Video was successfully updated." }
			else
				format.html { render :edit }
			end
		end
	end

	def destroy
		@video.destroy if @video.user == current_user || current_user.admin
		redirect_path = current_user.admin ? flagged_videos_path : setting_videos_path
		respond_to do |format|
			format.html { redirect_to redirect_path, notice: "Video was successfully destroyed." }
		end
	end

	def search
		query = params[:query].strip
		@videos = Video.search(query).paginate(:page => params[:page], :per_page => 10)
	end

	def setting
		@videos = current_user.videos
	end

	def favourite
		@video_subscriptions = current_user.video_subscriptions
	end

	def spoop_of_the_month
		@videos = Video.spooph_of_the_month_videos
	end

	def spoop_of_the_year
		@videos = Video.spooph_of_the_year_videos
	end

	def report_abuse
		@video.update(:reported => true)
		NotificationMailer.notify_abuse_video(@video, current_user)

		flash[:notice] = "Video has been reported successfully"
		redirect_to video_path(@video)
	end

	def remove_abuse_flag
		@video.update(:reported => false) if @video

		redirect_to flagged_videos_path
	end

	def flagged
		@videos = Video.reported_videos
	end

	def rate
		rating = Rating.where(:user_id => rating_params[:user_id], :video_id => rating_params[:video_id]).first_or_initialize
		rating.update(rating_params)

		respond_to do |format|
			format.json { render :json => {:status => 200} }
		end
	end

	def like
		VideoLike.where(:user => current_user, :video => @video).first_or_create
		respond_to do |format|
			format.html { redirect_to @video, :notice => "You have successfully liked video"}
		end
	end

	private

	def set_video
		@video = Video.find(params[:id])
	end

	def video_params
		params.require(:video).permit(:title, :video, :description).merge(user_id: current_user.id)
	end

	def themes_collection
		themes_ids = params.require(:video).permit(:themes => [])
		themes_ids.empty? ? [] : Theme.find([themes_ids["themes"]])
	end

	def rating_params
		params.permit(:video_id, :rate).merge(user_id: current_user.id)
	end
end
