class UserStatusesController < ApplicationController
	before_action :set_user_status

	def like
		status_like = UserStatusLike.where(:user_status => @user_status, :user => current_user).first_or_create

		respond_to do |format|
			format.json { render :json => {:likes => status_like.user_status.total_likes, :status => 200 } }
		end
	end

	def share
		UserStatusShare.where(:user_status => @user_status, :user => current_user).first_or_create unless current_user == @user_status.user
		
		redirect_to  profile_user_path(current_user)
	end

	private
	def set_user_status
		@user_status = UserStatus.find(params[:id])
	end
end
