module ApplicationHelper

	def resource_name
		:user
	end

	def resource
		@resource ||= User.new
	end

	def devise_mapping
		@devise_mapping ||= Devise.mappings[:user]
	end

	def calculate_strip_money(amount)
		((amount.round(2) + STRIPE_CONFIG[:extra_charge].round(2)) * 100).to_i
	end

	def deduct_retention_before_withdraw(amount)
		charges = amount * SETTINGS_CONFIG[:retention_percentage].to_f.round(2) / 100
		(amount - charges).round(2)
	end

	def stripe_withdraw_fee_adjustment(amount)
		(amount - SETTINGS_CONFIG[:stripe_withdraw_fee].to_f).round(2)
	end
end
