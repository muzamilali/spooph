class NotificationMailer < ActionMailer::Base
	default :from => MAILER_CONFIG[:from]

	def notify_abuse_video(video, user)
		@user = user
		@video = video

		mail(:to => SETTINGS_CONFIG[:admin_email], :subject => "Video has been reported of inappropriate content.").deliver
	end
end
