class SubscriptionMailer < ActionMailer::Base
	default :from => MAILER_CONFIG[:from]

	def free_subscription(subscription)
		@user = subscription.user
		@days = SETTINGS_CONFIG[:free_subscription_duration]

		mail(:to => @user.email, :subject => "Trial Subscription for #{@days} days").deliver
	end

	def paid_subscription(subscription)
		@user = subscription.user
		@days = SETTINGS_CONFIG[:paid_subscription_duration]

		mail(:to => @user.email, :subject => "Paid Subscription for #{@days} days").deliver
	end

	def new_video_notification(video, user)
		@video = video
		@user = user
		@channel = @video.channel

		mail(:to => user.email, :subject => " New video has been uploaded on #{@channel.name}").deliver
	end
end
