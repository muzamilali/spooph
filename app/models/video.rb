class Video < ActiveRecord::Base

	is_impressionable :counter_cache => true, :column_name => :impressions_count

	has_attached_file :video,
	styles: {
		:thumb => {
			:geometry => "125x100",
			:format => "jpeg",
			:time => 10
		},
		:medium => {
			:geometry => "250x200",
			:format => "jpeg",
			:time => 10
		},
		:large => {
			:geometry => "600x350",
			:format => "jpeg",
			:time => 10
		}
	},
	:processors => [:transcoder]

	# validates_attachment_content_type :video, :content_type => ["application/x-shockwave-flash", "application/x-shockwave-flash", "application/flv", "video/x-flv", "video/mp4", "video/mpeg", "video/webm", "video/avi", "video/ogg", "video/x-divx", "video/divx"]
	validates_attachment_content_type :video, content_type: /\Avideo\/.*\Z/

	validates_attachment_presence :video

	validates_presence_of :title

	belongs_to :user
	validates_presence_of :user_id

	has_many :video_likes

	belongs_to :channel

	has_and_belongs_to_many :themes

	has_many :comments
	has_many :ratings
	
	has_many :pledges

	has_many :video_subscriptions

	scope :spooph_of_the_month_videos, -> { where(:spooph_of_the_month => true) }
	scope :spooph_of_the_year_videos, -> { where(:spooph_of_the_year => true) }
	scope :reported_videos, -> { where(:reported => true) }

	validate :check_video_duration, :on => :create
	validate :trim_description, :on => :create
	validate :add_channel, :on => :create
	after_create :notify_subscribed_users

	def check_video_duration
		if self.video.queued_for_write[:original]

			properties = `ffmpeg -i #{self.video.queued_for_write[:original].path} 2>&1`
			duration_details = properties.match("Duration: ([0-9]+):([0-9]+):([0-9]+).([0-9]+)")
			
			duration = 1000
			duration = duration_details[1].to_i * 3600 + duration_details[2].to_i * 60 + duration_details[3].to_i if duration_details

			if not self.user.has_active_subscription? and not (30..60).include?(duration)
				errors.add(:video, " duration should be in between 30 seconds to 60 seconds")
			end

			if self.user.has_active_subscription? and not (30..180).include?(duration)
				errors.add(:video, " duration should be in between 30 seconds to 180 seconds")
			end

			self.duration = Time.at(duration).utc.strftime("%H:%M:%S")
		else
			errors.add(:video, " can't be blank")
		end
	end

	def trim_description
		self.description = self.description.strip
	end

	def add_channel
		self.channel = self.user.channel
	end

	def notify_subscribed_users
		users = self.user.being_followed_by
		users.each do |user|
			SubscriptionMailer.new_video_notification(self, user)
		end
	end

	def total_views
		self.impressionist_count(:filter => :session_hash)
	end

	def total_rating
		self.ratings.length == 0 ? 0 : (self.ratings.sum(:rate) / self.ratings.length).to_i
	end

	def total_comments
		self.comments.length
	end

	def self.most_viewed_videos
		Video.order(:impressions_count => :desc)
	end

	def self.search(param)
		where("title LIKE ? OR description LIKE ?", "%#{param}%", "%#{param}%")
	end

	def self.spooph_of_the_month
		previous_month = Date.today - 1.month
		start_date = previous_month.beginning_of_month
		end_date = previous_month.end_of_month
		
		impression = Impression.where(:created_at => start_date..end_date).group("impressionable_id").order("COUNT(*) DESC").limit(1).first
		if impression
			video = Video.find(impression.impressionable_id)
			video.update(:spooph_of_the_month => true)
		end
	end

	def self.spooph_of_the_year
		previous_year = Date.today - 1.year
		start_date = previous_year.beginning_of_year
		end_date = previous_year.end_of_year

		video = Video.spooph_of_the_month_videos.where(:created_at => start_date..end_date).order("impressions_count DESC").limit(1).first
		video.update(:spooph_of_the_year => true) if video
	end

	def self.remove_old_videos
		today = Date.today
		Video.all.each do |video|
			next if video.spooph_of_the_year

			time_diff = Time.diff(today, video.created_at)

			if video.spooph_of_the_month
				video.destroy if time_diff[:day] >= SETTINGS_CONFIG[:spooph_of_the_month_duration].to_i
			else
				video.destroy if not video.user.has_active_subscription? and time_diff[:day] > SETTINGS_CONFIG[:free_subscription_duration].to_i
				video.destroy if video.user.has_active_subscription? and time_diff[:day] > SETTINGS_CONFIG[:paid_subscription_duration].to_i
			end
		end
	end
end