class Comment < ActiveRecord::Base
	belongs_to :user
	belongs_to :video

	validates_presence_of :user_id
	validates_presence_of :video_id
	validates_presence_of :message

	belongs_to :parent, :class_name => "Comment", :foreign_key => "parent_id"
	has_many :replies, :class_name => "Comment", :foreign_key => "parent_id"

	scope :parent_comments, -> { where(:parent => nil) }

	before_save do
		self.message = self.message.strip
	end

	def complete_message
		self.removed? ? "This comment has been removed by user." : self.message
	end
end
