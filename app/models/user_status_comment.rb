class UserStatusComment < ActiveRecord::Base
	belongs_to :user
	belongs_to :user_status

	belongs_to :parent, :class_name => "UserStatusComment", :foreign_key => "parent_id"
	has_many :replies, :class_name => "UserStatusComment", :foreign_key => "parent_id"

	scope :parent_comments, -> { where(:parent => nil) }

	before_save do
		self.message = self.message.strip
	end

	def complete_message
		self.removed? ? "This comment has been removed by user." : self.message
	end
end
