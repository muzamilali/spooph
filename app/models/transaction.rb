class Transaction < ActiveRecord::Base
	belongs_to :user
	belongs_to :pledge

	validates_presence_of :user_id
	validates_presence_of :amount

	validate :check_amount, :on => [:create, :save]
	before_create :round_off_amount

	def check_amount
		errors.add(:amount, "Something is wrong with record.") if self.credit == self.debit
	end

	def round_off_amount
		self.amount = self.amount.round(2)
	end

end
