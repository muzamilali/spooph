class UserStatus < ActiveRecord::Base
	belongs_to :user

	has_many :user_status_comments

	has_many :user_status_likes
	has_many :user_status_shares

	validates_presence_of :body
	validates_presence_of :user_id

	attr_accessor :status_date

	after_create :share_with_followers

	def share_with_followers
		self.user.being_followed_by.collect{|user| UserStatusShare.where(:user_status => self, :user => user).create}
	end

	def total_likes
		self.user_status_likes.length
	end

end
