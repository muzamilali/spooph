class Pledge < ActiveRecord::Base
	belongs_to :user
	belongs_to :channel
	belongs_to :video

	validates_presence_of :user_id
	validates_presence_of :channel_id
	validates_presence_of :video_id

	validates :amount, :presence => true, :numericality => { :greater_than_or_equal_to => 2, :message => " should be greater than $2" }

	has_many :transactions

	before_create :round_off_amount
	
	def round_off_amount
		self.amount = self.amount.round(2)
	end

	def self.overall_pledged_amount
		where(:paid => true).sum(:amount)
	end	
end
