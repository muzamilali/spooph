class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# , :lockable, :timeoutable
	devise :database_authenticatable, :registerable, :confirmable,
		:recoverable, :rememberable, :trackable, :validatable, :omniauthable,
		:omniauth_providers => [:facebook, :google_oauth2]

	has_attached_file :photo,
		styles: {
			medium: "300x300>",
			thumb: "100x100>"
		}
	validates_attachment :photo, content_type: { content_type: /\Aimage\/.*\Z/ }

	has_attached_file :banner,
		styles: {
			medium: "850x280>",
			thumb: "400x400>"
		}
	validates_attachment :banner, content_type: { content_type: /\Aimage\/.*\Z/ }

	validates :username, :uniqueness => true, :presence => true, :allow_nil => false
	validates :name, :presence => true, :allow_nil => false

	has_many :identities

	has_one :channel

	has_many :videos
	has_many :video_likes
	
	has_many :comments

	has_many :ratings

	has_many :pledges
	
	has_many :transactions
	
	has_many :subscriptions

	has_many :sent_messages, :class_name => "Message", :foreign_key => "sender_id"
	has_many :received_messages, :class_name => "Message", :foreign_key => "recipient_id"

	has_many :user_statuses
	has_many :user_status_likes
	has_many :user_status_shares

	has_many :user_status_comments

	def self.find_or_create(auth)
		email = auth.info.email
		user = User.where(:email => email).first_or_initialize

		user.name ||= auth.info.name
		user.username ||= user.name.split(" ").first
		user.photo ||= open(auth.info.image, :allow_redirections => :safe)

		unless user.persisted?
			generated_password = Devise.friendly_token.first(8)
			user.password = generated_password
			user.password_confirmation = generated_password

			user.confirmed_at = DateTime.now
		end

		user.save

		user
	end

	def being_followed_by
		User.find (UserFollowing.where(:user_id => self.id).map(&:followed_by).uniq)
	end

	def following_other_users
		User.find (UserFollowing.where(:followed_by => self.id).map(&:user_id).uniq)
	end

	def self.subscribed_users
		all.select{|user| user.has_active_subscription?}
	end

	def self.subscribed_users_videos
		subscribed_users.collect{|user| user.videos}.flatten
	end

	def unpaid_pledges
		pledges.where(:paid => false)
	end

	def unpaid_pledges_amount
		unpaid_pledges.sum(:amount).round(2)
	end

	def paid_pledges
		pledges.where(:paid => true)
	end

	def paid_pledges_amount
		paid_pledges.sum(:amount).round(2)
	end

	def mark_unpaid_pledges
		pledges = self.pledges.where(:paid => false)

		pledges.each do |pledge|
			Transaction.create!(:user_id => pledge.video.user.id, :pledge_id => pledge.id, :amount => pledge.amount, :credit => true, :description => "Video pledged...")
		end

		pledges.update_all(:paid => true)
	end

	def money_owned
		transactions.where(:credit => true).sum(:amount) - transactions.where(:withdraw => true).sum(:amount)
	end

	def money_owned_after_system_deduction
		total_money = money_owned

		retention = total_money * SETTINGS_CONFIG[:retention_percentage].to_f.round(2) / 100
		remaining = total_money - retention - SETTINGS_CONFIG[:stripe_withdraw_fee].to_f.round(2)

		total_money == 0 ? total_money : remaining.round(2)
	end

	def add_debit_amount(amount)
		Transaction.create!(:user_id => self.id, :amount => amount, :debit => true, :withdraw => true, :description => "Withdraw amount")
	end

	def has_active_subscription?
		subscriptions.where(:active => true).length > 0
	end

	def have_subscription_money?
		self.money_owned >= SETTINGS_CONFIG[:spooph_subscription_fee].to_f.round(2)
	end

	def after_confirmation
		user = self
		today = Date.today

		days = SETTINGS_CONFIG[:free_subscription_duration].to_i.days
		subscription = Subscription.create!(:user_id => user.id, :start => today, :end => today + days)
		SubscriptionMailer.free_subscription(subscription)

		Channel.create!(:name => user.username, :user_id => user.id)
	end

	def recently_viewed_videos
		videos = []
		video_ids = Impression.where(:user_id => self.id).order("created_at desc").map(&:impressionable_id).uniq

		unless video_ids.empty?
			videos = Video.find(video_ids)

			videos = video_ids.collect{|id| videos.select{|video| video.id == id}.first }
		end

		videos.flatten
	end

	def personal_statuses
		self.user_statuses.order("created_at desc")
	end

	def user_shared_statuses
		self.user_status_shares.collect{|share| share.user_status.status_date = share.created_at and share.user_status}.sort{|a, b| b.status_date <=> a.status_date}
	end
	
end