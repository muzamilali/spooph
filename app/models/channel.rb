class Channel < ActiveRecord::Base
	belongs_to :user

	has_many :videos
	has_many :channel_subscriptions

	has_many :pledges

	validates_presence_of :user_id
	validates_presence_of :name

	def total_pledges
		pledges.sum(:amount)
	end
end
