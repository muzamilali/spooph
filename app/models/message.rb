class Message < ActiveRecord::Base

	belongs_to :sender, class_name: "User", primary_key: "id"
	belongs_to :recipient, class_name: "User", primary_key: "id"

	validates_presence_of :sender_id
	validates_presence_of :recipient_id
	validates_presence_of :subject
	validates_presence_of :body

	scope :unread_messages, -> { where(:read => false) }

	def self.remove_deleted_inbox_messages
		self.where(:recipient_trashed => false)
	end

	def self.remove_deleted_sent_messages
		self.where(:sender_trashed => false)
	end

	def self.only_deleted_messages(user)
		self.where("(sender_id = ? OR recipient_id = ?) and (sender_trashed = ? OR recipient_trashed = ?)", user.id, user.id, true, true)
	end
end
