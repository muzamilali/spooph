class UserFollowing < ActiveRecord::Base

	validates_presence_of :user_id
	validates_presence_of :followed_by

end
