jQuery.noConflict();
jQuery(function(){
	jQuery('#show').on('click',function(){
		jQuery('.card-reveal').slideToggle('slow');
	});

	jQuery('.card-reveal .close').on('click',function(){
		jQuery('.card-reveal').slideToggle('slow');
	});
});


// initialize video.js
// var my_video_id = videojs("my-video");

// Set value to the plugin
// my_video_id.watermark({
	
// 	file: 'images/logo.png',
	
// 	xpos: 50,
// 	ypos: 50,
// 	xrepeat: 0,
// 	opacity: 0.5
// });


jQuery.noConflict();
jQuery("[type=file]").on("change", function(){
	// Name of file and placeholder
	var file = this.files[0].name;
	var dflt = jQuery(this).attr("placeholder");
	if(jQuery(this).val()!=""){
		jQuery(this).next().text(file);
	} else {
		jQuery(this).next().text(dflt);
	}
});