jQuery.noConflict();
jQuery(document).on("ready page:load", function() {
	jQuery(".timeago").timeago();
});


jQuery.noConflict();
jQuery(function() {
	jQuery('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
jQuery.noConflict();
jQuery(function() {
	jQuery(window).bind("load resize", function() {
		topOffset = 50;
		width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
		if (width < 768) {
			jQuery('div.navbar-collapse').addClass('collapse');
			topOffset = 100; // 2-row-menu
		} else {
			jQuery('div.navbar-collapse').removeClass('collapse');
		}

		height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
		height = height - topOffset;
		if (height < 1) height = 1;
		if (height > topOffset) {
			jQuery("#page-wrapper").css("min-height", (height) + "px");
		}
	});

	var url = window.location;
	var element = jQuery('ul.nav a').filter(function() {
		return this.href == url || url.href.indexOf(this.href) == 0;
	}).addClass('active').parent().parent().addClass('in').parent();
	if (element.is('li')) {
		element.addClass('active');
	}
});


	jQuery.noConflict();
	jQuery(document).ready(function() {
	jQuery('.popup-form,.popup-youtube, .popup-gmaps').magnificPopup({
	disableOn: null,
	type: 'iframe',
	mainClass: 'mfp-fade',
	removalDelay: 200,
	preloader: false,
	fixedContentPos: true,
	});
	});
	
	
	jQuery.noConflict();
	jQuery(function() {
	//----- OPEN
	jQuery('[data-popup-open]').on('click', function(e)  {
		var targeted_popup_class = jQuery(this).attr('data-popup-open');
		jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(300);

		e.preventDefault();
	});


	//----- CLOSE
	jQuery(document).on("click", "[data-popup-close]", function(e) {
		var targeted_popup_class = jQuery(this).attr('data-popup-close');
		jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(300);

		e.preventDefault();
	});
});


jQuery.noConflict();
jQuery(document).ready(function(){
jQuery('.popup-close,.popup-close-overlay,.front-comment-btn').click(function(){
jQuery('video').removeAttr('src');
jQuery('.video-js')[0].player.pause().currentTime(0).trigger('loadstart');
	});
});


jQuery.noConflict();
jQuery(function () {
	jQuery('#carousel-example1').carousel({
		interval:4000,
		pause: 'false'
	});
	jQuery('#carousel-example2').carousel({
		interval:4000,
		pause: 'false'
	});
	jQuery(document).on("click", "#playButton", function() {
		jQuery('#carousel-example1, #carousel-example2').carousel('cycle');
	});
	jQuery(document).on("click", "#pauseButton", function() {
		jQuery('#carousel-example1, #carousel-example2').carousel('pause');
	});
	jQuery(document).on("click", "#prevButton", function() {
		jQuery('#carousel-example1, #carousel-example2').carousel('prev');
	});
	jQuery(document).on("click", "#nextButton", function() {
		jQuery('#carousel-example1, #carousel-example2').carousel('next');
	});
});


/*jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery(".cbp-spmenu-vertical,.comment-bar.comment-media-box").hover(function(){
		jQuery(this).css({"overflow": "auto"});
		}, function(){
		jQuery(this).css({"overflow": "hidden"});
	});
});
jQuery(document).ready(function(){
	jQuery(".comment-bar.comment-media-box").hover(function(){
		jQuery(".comment-bar.comment-media-box ul li").css({"width": "99%"});
		}, function(){
		jQuery(".comment-bar.comment-media-box ul li").css({"width": "100%"});
	});
});*/


jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery(".cbp-spmenu-vertical").hover(function(){
		jQuery(this).css({"overflow": "auto"});
		}, function(){
		jQuery(this).css({"overflow": "hidden"});
	});
});


	var nice = false;
	jQuery.noConflict();
/*	 jQuery(document).ready(function() {
		nice = jQuery("html,.comment-bar").niceScroll();
		}); */
		
		var obj = window;
		
		console.log(obj.length);
		console.log("selector" in obj);




jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery("#playlist li").click(function(){
	jQuery("#videoarea,#videoarea_html5_api").attr({
			"src": jQuery(this).attr("movieurl"),
			"poster": jQuery(this).attr("moviesposter"),
			"autoplay": "autoplay"
		})
	   
	});
});



jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery('.popup-close,.popup-close-overlay').click(function(){
	jQuery('input[type="checkbox"],input[type="file"]').attr('checked', false);
	   
	});
});



jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery("button#showLeftPush").click(function(){
		jQuery("#videoarea").toggleClass("video-size");
	});
	jQuery("button#showLeftPush").click(function(){
		jQuery(".vjs-watermark").toggleClass("watermark-resize");
	});
});



jQuery.noConflict();
jQuery(document).ready(function(){
	jQuery("#stars-default,#stars-default2").rating('create',{coloron:'#66D223'});
	jQuery("#stars-green").rating('create',{coloron:'gold'});
	jQuery("#stars-herats").rating('create',{coloron:'red',limit:10,glyph:'fa-heart'});	
});