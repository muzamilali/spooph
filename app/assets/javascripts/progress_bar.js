jQuery.noConflict();
(function ( jQuery ) {
	jQuery.fn.progress = function() {
		var percent = this.data("percent");
		this.css("width", percent + "%");
	};
}( jQuery ));

interval = setInterval(function(){
	jQuery(".bar-two .bar").progress();
}, 200);

jQuery.noConflict();
jQuery("#myElem").show().delay(1500).hide();
jQuery("#myElem").show().delay(1500).fadeOut();
setTimeout( function() { clearInterval(interval); }, 1000);