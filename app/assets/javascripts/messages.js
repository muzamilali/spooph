jQuery.noConflict();
jQuery(function(){

	jQuery(document).on("click", ".messages-box .delete-message", function(event) {
		event.preventDefault();

		var message_id = jQuery(this).attr("data-target");
		
		jQuery.ajax({
			url: "/messages/" + message_id,
			data: { },
			method: "DELETE",
			success: function(data) {
				$(".messages-box").replaceWith(data.content);
			},
			error: function() { }
		});
	});

});
