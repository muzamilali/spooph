//= require classie
//= require menu_classy
//= require pop_ups
//= require video

//= require videojs.ads
//= require videojs.ima
//= require videojs.watermark
//= require video_card

//= require scripts
//= require bootstrap
//= require bootstrap-star-rating
//= require rate_video

//= require comments
//= require messages
//= require user_status_comments
//= require pledges
//= require user_statuses
// require bootstrap-editable
//= require users
