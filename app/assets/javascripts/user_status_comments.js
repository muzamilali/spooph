jQuery.noConflict();
jQuery(function(){

	jQuery(document).on("click", ".new-user-status-comment-create button", function(event) {
		event.preventDefault();

		var form = jQuery(this).parents("form");
		var user_status_id = jQuery("#user_status_comment_user_status_id", form).val();
		var parent_id = null;
		
		var message = jQuery("#user_status_comment_message", form).val();
		message = jQuery.trim(message);
		if (message == "") return;
		
		jQuery.ajax({
			url: "/user_status_comments",
			data: {
				user_status_comment: {
					message: message,
					user_status_id: user_status_id,
					parent_id: parent_id
				}
			 },
			method: "POST",
			success: function(data) {
				jQuery("#content-status-comments-" + user_status_id).html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".toggle-status-comment-reply", function(event) {
		id = jQuery(this).attr("data-target");
		jQuery(id).toggle();
	});


	jQuery(document).on("click", ".status-comment-reply-button button", function(event) {
		event.preventDefault();

		var form = jQuery(this).parents("form");

		var user_status_id = jQuery("#user_status_comment_user_status_id", form).val();
		var parent_id = jQuery("#user_status_comment_parent_id", form).val();
		
		var message = jQuery("#user_status_comment_message", form).val();
		message = jQuery.trim(message);
		if (message == "") return;
		
		jQuery.ajax({
			url: "/user_status_comments",
			data: {
				user_status_comment: {
					message: message,
					user_status_id: user_status_id,
					parent_id: parent_id
				}
			 },
			method: "POST",
			success: function(data) {
				jQuery("#content-status-comments-" + user_status_id).html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".delete-status-comment", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");

		var parent = jQuery(this).parents(".popup-inner")[0];
		var user_status_id = parent.id;
		
		jQuery.ajax({
			url: "/user_status_comments/" + id,
			data: { },
			method: "DELETE",
			success: function(data) {
				jQuery("#" + user_status_id).html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".edit-status-comment-btn", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");
		jQuery(".status-text-" + id).toggle();
	});

	jQuery(document).on("click", ".update-status-comment-btn", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");

		var parent = jQuery(this).parents(".popup-inner")[0];
		var user_status_id = parent.id;

		var message = jQuery("textarea", jQuery(this).siblings("div")).val();
		message = jQuery.trim(message);
		if (message == "") return;
		
		jQuery.ajax({
			url: "/user_status_comments/" + id,
			data: {
				user_status_comment : {
					message: message
				}
			},
			method: "PUT",
			success: function(data) {
				jQuery("#" + user_status_id).html(data.content);
			},
			error: function() { }
		});
	});
});
