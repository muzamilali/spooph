jQuery.noConflict();
jQuery(function(){

	jQuery(document).on("click", ".new-comment-create button", function(event) {
		event.preventDefault();

		var form = jQuery(this).parents("form");
		var video_id = jQuery("#comment_video_id",form).val();
		var parent_id = null;
		
		var message = jQuery("#comment_message",form).val();
		message = jQuery.trim(message);
		if (message == "") return;
		
		jQuery.ajax({
			url: "/comments",
			data: {
				comment: {
					message: message,
					video_id: video_id,
					parent_id: parent_id
				}
			 },
			method: "POST",
			success: function(data) {
				jQuery("#video-comments-popup-content").html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".toggle-reply", function(event) {
		id = jQuery(this).attr("data-target");
		jQuery(id).toggle();
	});


	jQuery(document).on("click", ".comment-reply-button button", function(event) {
		event.preventDefault();

		var form = jQuery(this).parents("form");
		var video_id = jQuery("#comment_video_id",form).val();
		var parent_id = jQuery("#comment_parent_id",form).val();

		var message = jQuery("#comment_message",form).val();
		message = jQuery.trim(message);
		if (message == "") return;
		
		jQuery.ajax({
			url: "/comments",
			data: {
				comment: {
					message: message,
					video_id: video_id,
					parent_id: parent_id
				}
			 },
			method: "POST",
			success: function(data) {
				jQuery("#video-comments-popup-content").html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".delete-comment", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");
		
		jQuery.ajax({
			url: "/comments/" + id,
			data: { },
			method: "DELETE",
			success: function(data) {
				jQuery("#video-comments-popup-content").html(data.content);
			},
			error: function() { }
		});
	});

	jQuery(document).on("click", ".edit-comment-btn", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");
		jQuery(".message-text-" + id).toggle();
	});

	jQuery(document).on("click", ".update-comment-btn", function(event) {
		event.preventDefault();

		var id = jQuery(this).attr("data-id");
		var message = jQuery("textarea", jQuery(this).siblings("div")).val();
		message = jQuery.trim(message);

		if (message == "") return;
		
		jQuery.ajax({
			url: "/comments/" + id,
			data: {
				comment : {
					message: message
				}
			},
			method: "PUT",
			success: function(data) {
				jQuery("#video-comments-popup-content").html(data.content);
			},
			error: function() { }
		});
	});
});
