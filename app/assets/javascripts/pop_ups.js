jQuery.noConflict();
jQuery('.upload-click').click(function() {
	jQuery('.popup').css({'z-index': '999999999', 'display': 'none'});
	jQuery('.popup.popup-upload').css({'z-index': '9999999999'});
});

jQuery('.sign-up-click').click(function() {
	jQuery('.popup').css({'z-index': '999999999', 'display': 'none'});
	jQuery('.popup.popup-signup').css({'z-index': '9999999999'});
});

jQuery('.signin-click').click(function() {
	jQuery('.popup').css({'z-index': '999999999', 'display': 'none'});
	jQuery('.popup.popup-login').css({'z-index': '9999999999'});
});

jQuery('.forgot-pass-click').click(function() {
	jQuery('.popup').css({'z-index': '999999999', 'display': 'none'});
	jQuery('.popup.popup-forgot').css({'z-index': '9999999999'});
});