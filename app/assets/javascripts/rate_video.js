jQuery.noConflict();
jQuery(function(){

	jQuery(".rate-this-video .ratingicon").on("click", function() {
		var form = jQuery(this).parents("form");
		var rating = jQuery("input[name=rating]", form).val();
		var video_id = jQuery("input[name=video]", form).val();
		var url = "/videos/rate";
		
		jQuery.ajax({
			url: url,
			data: {
				rate: rating,
				video_id: video_id
			},
			method: "post",
			success: function(data) {
				alert("Video has been successfully rated.");
			},
			error: function() {

			}
		});
	});

});