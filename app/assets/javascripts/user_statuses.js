jQuery.noConflict();
jQuery(function(){

	var tag = null;

	jQuery(document).on("click", ".status-like-button", function(event) {
		event.preventDefault();

		tag = jQuery(this);
		var url = jQuery(this).attr("href");

		jQuery.ajax({
			url: url,
			data: { },
			method: "GET",
			success: function(data) {
				tag.html('<i class="fa fa-heart"></i> ' + data.likes + " Likes");
			},
			error: function() { }
		});
	});
});