class CreateJoinTable < ActiveRecord::Migration
	def change
		create_join_table :themes, :videos do |t|
			# t.index [:theme_id, :video_id]
			# t.index [:video_id, :theme_id]
		end
	end
end
