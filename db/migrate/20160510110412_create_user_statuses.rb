class CreateUserStatuses < ActiveRecord::Migration
  def change
    create_table :user_statuses do |t|
      t.text :body
      t.integer :user_id

      t.timestamps
    end
  end
end
