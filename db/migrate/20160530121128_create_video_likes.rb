class CreateVideoLikes < ActiveRecord::Migration
  def change
    create_table :video_likes do |t|
      t.integer :user_id
      t.integer :video_id

      t.timestamps
    end
  end
end
