class CreateSubscriptions < ActiveRecord::Migration
	def change
		create_table :subscriptions do |t|
			t.date :start
			t.date :end

			t.boolean :active, :default => true
			
			t.integer :user_id

			t.timestamps
		end
	end
end
