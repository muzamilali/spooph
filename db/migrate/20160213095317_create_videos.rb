class CreateVideos < ActiveRecord::Migration
	def change
		create_table :videos do |t|
			t.string :title
			t.text :description
			t.attachment :video
			t.time :duration

			t.integer :impressions_count

			t.integer :user_id
			t.integer :channel_id

			t.boolean :spooph_of_the_month, :default => false
			t.boolean :spooph_of_the_year, :default => false
			
			t.boolean :reported, :default => false

			t.timestamps
		end
	end
end
