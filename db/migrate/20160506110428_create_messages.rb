class CreateMessages < ActiveRecord::Migration
	def change
		create_table :messages do |t|
			t.string :subject
			t.text :body

			t.integer :sender_id
			t.integer :recipient_id

			t.boolean :read, :default => false
			
			t.boolean :sender_trashed, :default => false
			t.boolean :recipient_trashed, :default => false

			t.timestamps
		end
	end
end
