class AddFieldsToUser < ActiveRecord::Migration
	def change
		add_column :users, :name, :string
		add_column :users, :username, :string
		
		add_column :users, :bio, :text

		add_column :users, :account, :string
		add_column :users, :routing_number, :string
		add_column :users, :country, :string, :default => "US"

		add_column :users, :admin, :boolean, :default => false
	end
end
