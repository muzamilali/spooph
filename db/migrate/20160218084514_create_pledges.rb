class CreatePledges < ActiveRecord::Migration
	def change
		create_table :pledges do |t|

			t.float :amount
			t.boolean :paid, :default => false

			t.integer :user_id
			t.integer :channel_id
			t.integer :video_id

			t.timestamps
		end
	end
end
