class CreateComments < ActiveRecord::Migration
	def change
		create_table :comments do |t|
			t.string :message
			t.integer :user_id
			t.integer :video_id
			t.integer :parent_id

			t.boolean :removed, :default => false

			t.timestamps
		end
	end
end
