class CreateUserStatusLikes < ActiveRecord::Migration
	def change
		create_table :user_status_likes do |t|
			t.integer :user_id
			t.integer :user_status_id

			t.timestamps
		end
	end
end
