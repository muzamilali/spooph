class CreateThemes < ActiveRecord::Migration
	def change
		create_table :themes do |t|
			t.string :name
			t.string :html_class

			t.timestamps
		end
	end
end
