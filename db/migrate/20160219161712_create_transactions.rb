class CreateTransactions < ActiveRecord::Migration
	def change
		create_table :transactions do |t|

			t.integer :user_id
			t.integer :pledge_id

			t.float :amount
			
			t.text :description

			t.boolean :credit, :default => false
			t.boolean :debit, :default => false
			
			t.boolean :withdraw, :default => false

			t.timestamps
		end
	end
end
