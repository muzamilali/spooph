class CreateUserFollowings < ActiveRecord::Migration
	def change
		create_table :user_followings do |t|
			
			t.integer :user_id
			t.integer :followed_by

			t.timestamps
		end
	end
end
