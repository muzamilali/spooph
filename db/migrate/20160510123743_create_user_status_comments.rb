class CreateUserStatusComments < ActiveRecord::Migration
  def change
    create_table :user_status_comments do |t|
      t.text :message
      t.integer :user_id
      t.integer :parent_id
      t.integer :user_status_id
      t.boolean :removed

      t.timestamps
    end
  end
end
